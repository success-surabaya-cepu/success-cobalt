<?php

use App\Http\Controllers\HomeController;
use App\Http\Middleware\IsUserValidated;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'welcome']);

Route::middleware(['auth:sanctum', 'verified', IsUserValidated::class])->group(function (){
    Route::get('/dashboard', [HomeController::class, 'dashboard'])->name('dashboard');
});
