<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    use HasFactory;
    protected $fillable = [];

    public function user_created_by(){
        return $this->belongsTo(User::class, 'created_by');
    }

    public function user_updated_by(){
        return $this->belongsTo(User::class, 'updated_by');
    }

    public function user_right(){
        return $this->hasMany(UserRight::class);
    }

    public function question_group(){
        return $this->hasMany(QuestionGroup::class);
    }
}
