<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use HasFactory;
    protected $fillable = [];

    public function question_group(){
        return $this->belongsTo(QuestionGroup::class);
    }

    public function user(){
        return $this->belongsTo(User::class, 'created_by');
    }

    public function question_attribute(){
        return $this->belongsTo(QuestionAttribute::class);
    }

    public function answer_option(){
        return $this->hasMany(AnswerOption::class);
    }
}
