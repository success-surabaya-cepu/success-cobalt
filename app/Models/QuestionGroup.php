<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuestionGroup extends Model
{
    use HasFactory;
    protected $fillable = [];

    public function user_created_by(){
        return $this->belongsTo(User::class, 'created_by');
    }

    public function user_updated_by(){
        return $this->belongsTo(User::class, 'updated_by');
    }

    public function test(){
        return $this->belongsTo(Test::class);
    }

    public function user_question_session(){
        return $this->hasMany(UserQuestionSession::class);
    }

    public function question(){
        return $this->hasMany(Question::class);
    }


}
