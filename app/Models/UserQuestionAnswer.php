<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserQuestionAnswer extends Model
{
    use HasFactory;
    protected $fillable = [];

    public function answer_option(){
        return $this->belongsTo(AnswerOption::class);
    }

    public function user_question_session(){
        return $this->belongsTo(UserQuestionSession::class);
    }
}
