<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserQuestionSession extends Model
{
    use HasFactory;
    protected $fillable = [];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function question_group(){
        return $this->belongsTo(QuestionGroup::class);
    }
}
