<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Inertia\Inertia;

class IsUserValidated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(auth()->user()->validated == 0){
            return (auth()->user()->type == "host")
                ? Inertia::render("Host/UnvalidatedHost")
                : Inertia::render("Participant/UnvalidatedParticipant");
        }else{
            return $next($request);
        }
    }
}
