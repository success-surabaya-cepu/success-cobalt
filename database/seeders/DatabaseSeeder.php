<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory()->create([
            'name' => 'Host SuperUser',
            'email' => 'admin@test.com',
            'type' => 'host',
            'validated' => 1,
        ]);

        \App\Models\User::factory()->create([
            'name' => 'Participant Dummy',
            'email' => 'dummy@test.com',
            'type' => 'participant',
            'validated' => 1,
        ]);
    }
}
